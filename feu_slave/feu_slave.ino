/* include libraries */
  #include <SoftwareSerial.h>
  #include <bluetooth.h>
  #include <feu.h>
  #include <ultrason.h>
  #include <luminosite.h>

/* pin define */
 /** Bluetooth **/
  const byte RxD = 10; // to TxD HC-05 Module
  const byte TxD = 11; // to RxD HC-05 Module
  const byte Key = 9;

  /** Feux Tricolore **/
  const int LV = 6;
  const int LO = A1;
  const int LR = A2;
  const byte buttonPin = 13;

  /** Luminosite **/
  const int LightPin = A0;

  /** Ultrason **/ 
  const byte triggerPin = 2;
  const byte echoPin = 3;

/* Constante define */
  SoftwareSerial BTSerial(RxD,TxD);
  Bluetooth BT(RxD,TxD,Key,&BTSerial, 9600);
  Feu feu(LV, LO, LR);
  Ultrason ultra(triggerPin, echoPin);
  Luminosite lum(LightPin);

  int warningCount = 0;
  
void setup() {
  BT.init();
  feu.init();
  ultra.init();
  lum.init();

  pinMode(buttonPin, INPUT);
  Serial.begin(9600);

/** Initialiser le module HC-05 en esclave**/
  Serial.println("Debut de l'initialisation");
  Serial.println(BT.CMD("AT+NAME=HC05-slave"));
  Serial.println(BT.CMD("AT+PSWD=1234"));
  Serial.println(BT.CMD("AT+ROLE=0"));
  Serial.println(BT.CMD("AT+CMODE=0"));
  // Supprime les anciennes conenxions misent en cache
  Serial.println(BT.CMD("AT+RMAAD"));
  
  // mode warning tant que l'arduino maître n'a pas débuté son programme
  feu.setMode(WARNING);
}

void loop() {
  // Reception du message pour changements internes
  
  String msg = "";
  if(BT.available()){
    msg = BT.BTread(); 
    Serial.println(msg);
  }
  
  String key="";
  String value="";
  float luminosite = lum.getLuminosite();

  // fonction pour récupérer la valeu donné après ":"
  for(int i=0;i<msg.length();i++){
    if(msg[i] == ':'){
      key=msg.substring(0,i);
      value=msg.substring(i+1,i+2);
      warningCount=0;
      feu.setMode(NORMAL);
      break;
    }
  }
  if(key.equals("MODE")){
    feu.setMode(value.toInt());
  } else if(key.equals("COLOR")){
    feu.setCouleur(value.toInt());
  }
  else {
    // si il n'y a pas eu de réponse depuis x temps, mode warning
    if(warningCount > 80){
      feu.setMode(WARNING);
    }
    delay(100);
    warningCount++;
  }

  if(feu.getMode() == WARNING){
      // mode WARNING
      feu.setCouleur(ORANGE);
      feu.updateLuminosite(luminosite);
      delay(200);
      feu.setCouleur(AUCUNE);
  }

  feu.updateLuminosite(luminosite);

  // Emission des données

  int statusPieton = digitalRead(buttonPin);
  if(statusPieton == HIGH){
    Serial.println("PIETON");
    BT.println("PIETON");
  }
  
  float voiture = ultra.getDistance_cm();
  if(voiture > 15.0 && voiture < 100.0 && feu.getMode()== NUIT){
    Serial.println(String(voiture));
    BT.println("VOITURE");
  }
}
