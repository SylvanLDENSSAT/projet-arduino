#include "luminosite.h"

// 
Luminosite::Luminosite(int pin){
  // broche pour capteur de lumninosite ambiente
  lightPin = pin;
}

void Luminosite::init(){
  pinMode(lightPin, INPUT);
}

// calcul la luminosite du feu adapte à la luminosite ambiante
float Luminosite::getLuminosite(){
  // recupere la luminosite ambiante dans [0, 1023]
  return analogRead(lightPin) / 1023.0;
}
