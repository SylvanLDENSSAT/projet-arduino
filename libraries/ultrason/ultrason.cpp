#include "ultrason.h"

Ultrason::Ultrason(byte trigger, byte echo){
    // broche pour le module ultrason
    _trigger = trigger;
    _echo = echo;
}

void Ultrason::init(){
/* Initialise les broches */
    pinMode(_trigger, OUTPUT);
    // La broche TRIGGER doit être à LOW au repos
    digitalWrite(_trigger, LOW); 
    pinMode(_echo, INPUT);
}

float Ultrason::getDistance_mm(){
/* 1. Lance une mesure de distance en envoyant 
 * une impulsion HIGH de 10µs sur la broche TRIGGER 
 */
    digitalWrite(_trigger, HIGH);
    delayMicroseconds(10);
    digitalWrite(_trigger, LOW);
    
/* 2. Mesure le temps entre l'envoi de l'impulsion 
 * ultrasonique et son écho (si il existe) 
 */
    long measure = pulseIn(_echo, HIGH, MEASURE_TIMEOUT);
    
/* 3. Calcul la distance à partir du temps mesuré */
    float distance_mm = measure / 2.0 * SOUND_SPEED;
    
    return distance_mm;
}

float Ultrason::getDistance_cm(){
    return Ultrason::getDistance_mm()/10.0;
}

float Ultrason::getDistance_m(){
    return Ultrason::getDistance_mm()/1000.0;
}
