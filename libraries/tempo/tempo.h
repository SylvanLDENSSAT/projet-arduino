#ifndef tempo
#define tempo

// PLUS PETITE FRACTION TEMPORELLE
const int dt = 300;

// RAPPORT ENTRE 1 SEC ET dt, (X * s * dt = 1000)
const float s = 1000 / dt;

#endif
