#include "bluetooth.h"

Bluetooth::Bluetooth(byte RxD, byte TxD, byte Key, SoftwareSerial* BTSerial, int baud){
    _RxD = RxD;
    _TxD = TxD;
    _Key = Key;
    _baud = baud;
    
    _BTSerial = BTSerial;
}

void Bluetooth::init(){
    pinMode(_RxD, INPUT);
    pinMode(_TxD, OUTPUT);
    pinMode(_Key, OUTPUT);

    _BTSerial->begin(_baud);
}

void Bluetooth::println(String msg){
    _BTSerial->listen();
    digitalWrite(_Key, LOW);
    _BTSerial->println(msg);
}

void Bluetooth::print(String msg){
    _BTSerial->listen();
    digitalWrite(_Key, LOW);
    _BTSerial->print(msg);
}

String Bluetooth::CMD(String msg){
    _BTSerial->listen();
    digitalWrite(_Key, HIGH);
    _BTSerial->println(msg);
    return Bluetooth::BTread();
}
/*
String Bluetooth::BTread(){
    _BTSerial->listen();
    String msg = "";
    int delay = 200;
    char c;
    while(!_BTSerial->available() || delay == 0){Serial.println(delay--);}
    //delay(200);
    while(_BTSerial->available()){
        c = _BTSerial->read();
        if(c != '\r' && c != '\n'){
            msg = String(msg + c);
        }
    }
    return msg;
/*    while(!_BTSerial->available()){}
    while(_BTSerial->available()){
        msg = _BTSerial->readString();
    }
    return msg.substring(0,msg.length()-2);

}*/
String Bluetooth::BTread(){
    _BTSerial->listen();
    String msg = "";
    char c;
    while(c != '\n'){
        if(_BTSerial->available()){
            c = _BTSerial->read();
            if(c != '\r' && c != '\n'){
                msg = String(msg + c);
            }
        }
    }
    return msg;
}

String Bluetooth::BTreadbis(){
    _BTSerial->listen();
    String msg = "";
    char c;
    //while(!_BTSerial->available() || delay == 0){Serial.println(delay--);}
    delay(200);
    while(_BTSerial->available()){
        c = _BTSerial->read();
        if(c != '\r' && c != '\n'){
            msg = String(msg + c);
        }
    }
    return msg;
}
bool Bluetooth::available(){
    _BTSerial->listen();
    return _BTSerial->available();
}

char Bluetooth::read(){
    _BTSerial->listen();
    return _BTSerial->read();
}