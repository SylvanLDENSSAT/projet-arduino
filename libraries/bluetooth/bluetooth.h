#ifndef bluetooth
#define bluetooth

#include "Arduino.h"
#include <SoftwareSerial.h>

class Bluetooth{
  public:
    Bluetooth(byte RxD, byte TxD, byte Key, SoftwareSerial* BTSerial, int baud);
    void init();
    void println(String msg);
    void print(String msg);
    String CMD(String msg);
    String BTread();
    String BTreadbis();
    char read();
    bool available();
  private:
    byte _RxD;
    byte _TxD;
    byte _Key;
    int _baud;
    SoftwareSerial* _BTSerial;
};

#endif
