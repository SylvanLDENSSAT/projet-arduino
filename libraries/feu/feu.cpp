#include "feu.h"

// constructeur
Feu::Feu(int LV, int LO, int LR){
  // 6 5 3
  _LV = LV; // broche diode verte
  _LO = LO; // broche diode orange
  _LR = LR; // broche diode rouge
}

void Feu::init(){
  pinMode(_LV, OUTPUT);
  pinMode(_LO, OUTPUT);
  pinMode(_LR, OUTPUT);
}

// renvoie 
int Feu::getCouleur(){
  // renvoie la couleur actuelle du feu
  return _couleur;
}

void Feu::setCouleur(int couleur){
  Serial.println("couleur change");
  _couleur = couleur;
}

int Feu::getMode(){
  return _mode;
}

void Feu::setMode(int mode){
  _mode = mode;
}

void Feu::updateLuminosite(float luminosite){
  // TODO : petit calcul pour adapter la luminosite
  int mini = 130;
  int maxi = 255;
  int luminosite_feu = 255; //mini + (maxi - mini) * luminosite;
  //
  switch(_couleur){
    case ROUGE:
      analogWrite(_LR, luminosite_feu);
      digitalWrite(_LO, 0);
      digitalWrite(_LV, 0);
      break;
    case ORANGE:
      analogWrite(_LO, luminosite_feu);
      digitalWrite(_LV, 0);
      digitalWrite(_LR, 0);
      break;
    case VERT:
      analogWrite(_LV, luminosite_feu);
      digitalWrite(_LO, 0);
      digitalWrite(_LR, 0);
      break;
    case AUCUNE:
      digitalWrite(_LV, 0);
      digitalWrite(_LO, 0);
      digitalWrite(_LR, 0);
      break;
  }
}
