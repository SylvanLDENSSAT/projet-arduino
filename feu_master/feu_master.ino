/* include libraries */
#include <SoftwareSerial.h>
#include <tempo.h>
#include <bluetooth.h>
#include <feu.h>
#include <luminosite.h>

#include <LiquidCrystal_I2C.h>
#include <Wire.h> 

/* pin define */
 /** Bluetooth **/
  const byte RxDM = 5; // to TxD HC-05 Module
  const byte TxDM = 6; // to RxD HC-05 Module
  const byte KeyM = 7;

  const byte RxDA = 10;
  const byte TxDA = 11;
  const byte KeyA = 12; 

  /** Feux Tricolore **/
  const int LV = A3;
  const int LO = A2;
  const int LR = 3;
  const byte pietonPin = 13; // simulation du bouton pieton

  /** Luminosite **/
  const int LightPin = A0;

/* Constante define */
  SoftwareSerial Master(RxDM,TxDM);
  SoftwareSerial Admin(RxDA,TxDA);
  Bluetooth BTMaster(RxDM,TxDM,KeyM,&Master, 9600);
  Bluetooth BTAdmin(RxDA,TxDA,KeyA,&Admin, 9600);
  LiquidCrystal_I2C lcd(0x20,16,2); 
  
  
  Feu feu(LV, LO, LR);
  Luminosite lum(LightPin);

  // compte le nombre de ms ecoulees depuis le lancement du programme
  int C = 0;

  
void setup() {
  Serial.begin(9600);

  lum.init();
  feu.init();
  BTMaster.init();
  BTAdmin.init();

  lcd.init(); // initialize the lcd 
  lcd.backlight();
  pinMode(A1,INPUT);
  
  //Paramétrage du bluetooth “Admin” (mode slave, pwd=9999, name=admin)
  Serial.println("parametrage du BT Admin");
  Serial.println(BTAdmin.CMD("AT+ROLE=0"));
  Serial.println(BTAdmin.CMD("AT+PSWD=9999"));
  Serial.println(BTAdmin.CMD("AT+NAME=HC05-Admin"));
  Serial.println(BTAdmin.CMD("AT+RMAAD"));
  

  //Le maître paramètre le second bluetooth “Master” 
  //(mode master, pwd=1234, name=master)
  Serial.println("parametrage du BT Master");
  Serial.println(BTMaster.CMD("AT+ROLE=1"));
  Serial.println(BTMaster.CMD("AT+PSWD=1234"));
  Serial.println(BTMaster.CMD("AT+NAME=HC05-Master"));
  Serial.println(BTMaster.CMD("AT+RMAAD"));
  Serial.println(BTMaster.CMD("AT+CMODE=1"));

  String msg = "";
  String mac = "";
  Serial.println("En attente de la transmission du module bluetooth");
  while(!msg.startsWith("PAIRED")){
    // Si on reçoie une chaîne via le smartphone
    if(BTAdmin.available()){
      // On enregistre la chaîne dans la variable "msg"
      msg = BTAdmin.BTread();
      Serial.println(msg);
    }

    // Si on reçoie une chaîne commençant par "MAC:"
    if(msg.startsWith("MAC:")){
      // On extrait l'adresse en retirant "MAC:"
      mac = msg.substring(4,msg.length());
      // On vide "msg" pour ne pas retourner dans la condition
      msg = "";
      // envoie des commandes pour paramétrer le bluetooth BTMaster
      Serial.println();
      Serial.println("init (ERROR (27) : OK) : "+BTMaster.CMD("AT+INIT"));
      Serial.println("rname : "+ BTMaster.CMD("AT+RNAME?"+mac));
      // FAIL / OK      
      Serial.println("link : "+ BTMaster.CMD("AT+LINK="+mac));
    }
  }  

  Serial.println("debut du loop");
  feu.setMode(NORMAL);
}
int count=0;
void loop() {

  // Lecture de la luminosité ambiante
  float luminosite = lum.getLuminosite(); // dans [0, 1]
  
  // Lecture du bouton des piétons du feu maître
  int pieton = digitalRead(pietonPin);

  // recupere les messages sur la liaison bluetooth
  String msg = "";
  if(BTMaster.available()) msg = BTMaster.BTread();
  if(msg != "") Serial.println(msg);
  
  // mode actualisé uniquement quand le feu maître est VERT
  if(luminosite < LUMINOSITE_JOUR && feu.getCouleur()==VERT){
    // paramètre le feu en mode NUIT
    feu.setMode(NUIT);
    // transmission du mode à l'esclave
    BTMaster.println("MODE:1");
  }else if(luminosite > LUMINOSITE_JOUR && C%TTOTAL==0){
    // paramètre le feu en mode NORMAL
    feu.setMode(NORMAL);
    // transmission du mode à l'esclave
    BTMaster.println("MODE:0");
  } 


  if(pieton==HIGH && feu.getCouleur()==VERT){
    // pieton a appuyé sur le bouton, on passe au temps du orange
    C = TV;
    feu.setMode(NORMAL);
  }
  else if(msg.startsWith("PIETON") && feu.getMode()==NORMAL && feu.getCouleur()==ROUGE){
    // le feu esclave passe à l'orange
    C = 2*TV+TO;
    feu.setMode(NORMAL);
  }

  // Le maître reçoie qu'il y a une voiture en attente.
  if(msg.startsWith("VOITURE") && feu.getMode()==NUIT){
    // le feu esclave maître passe à l'orange
    C = TV;
    // le feu repasse en NORMAL pour faire écouler le cycle
    // qui change la couleur des feux
    feu.setMode(NORMAL);
  }
 

  
  // gestion de la couleur, selon le mode
  if(feu.getMode() == NORMAL){
    // Traitement du mode NORMAL
    if(C%TTOTAL==0){
      // maître VERT
      feu.setCouleur(VERT);
      // esclave ROUGE
      BTMaster.println("COLOR:0");   
    }else if(C%TTOTAL==TV){
      // maître ORANGE
      feu.setCouleur(ORANGE);
      // esclave ROUGE
      BTMaster.println("COLOR:0");
    }else if(C%TTOTAL==TV+TO){
      // maître ROUGE
      feu.setCouleur(ROUGE);
      // envoyer VERT
      BTMaster.println("COLOR:2");
    }else if(C%TTOTAL==2*TV+TO){
      // maître toujours ROUGE
      // envoyer ORANGE
      BTMaster.println("COLOR:1");
    }
  }else if(feu.getMode() == NUIT){
    // Traitement du mode NUIT
    if(C % 5 == 0){
      // feu VERT
      feu.setCouleur(VERT);
      // envoyer ROUGE
      BTMaster.println("COLOR:0");
    }
  }else if(feu.getMode() == WARNING){
    // Traitement du mode WARNING
    if(C%(2*TW)==0){
      feu.setCouleur(ORANGE);
    }else if(C%(2*TW)==TW){
      feu.setCouleur(AUCUNE);
    }    
  }
  
  // change la couleur du feu et sa luminosité
  feu.updateLuminosite(luminosite);

  // actualisation de la température tout les 25 dt (i.e dt = 300 ms)
  if(C%25 == 0){
    // Mesure la température sans modifications 
    float valeur_brute = analogRead(A1);
    
    // Transforme la mesure en température en Celcius
    float temperature_celcius = valeur_brute * (5.0 / 1023.0 * 100.0);

    // ligne 1, ligne 2
    prompt("temperature : ", String(temperature_celcius)+" Celcius");
  }
  
  delay(dt);
  C++;
}

void prompt(String ligne1, String ligne2){
  // efface l'ancien écran
  lcd.clear();
  // écrit sur la première ligne
  lcd.print(ligne1);
  // se décale à la ligne 2
  lcd.setCursor(0, 1);
  // ecrit sur la seconde ligne
  lcd.print(ligne2);
}
